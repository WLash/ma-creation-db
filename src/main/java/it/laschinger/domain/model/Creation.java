package it.laschinger.domain.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name="creation", uniqueConstraints={@UniqueConstraint(columnNames = {"title", "owner_id"})})
public class Creation implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull
	@Size (min=3, max=45)
	private String title;

	@NotNull
	@ManyToOne
	private User owner;

	@OneToMany(mappedBy = "creation")
	@JsonManagedReference
	private Set<Share> shareSet = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull
	public String getTitle() {
		return title;
	}

	public void setTitle(@NotNull String title) {
		this.title = title;
	}

	@NotNull
	public User getOwner() {
		return owner;
	}

	public void setOwner(@NotNull User owner) {
		this.owner = owner;
	}

	public Set<Share> getShareSet() {
		return shareSet;
	}

	public void setShareSet(Set<Share> shareSet) {
		this.shareSet = shareSet;
	}

	public String toString() {
		return "User{ id = " + id + ", title = " + title + "}";
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof Creation && Objects.equals(id, ((Creation) obj).id);
	}
}
