package it.laschinger.domain.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name="user", uniqueConstraints={@UniqueConstraint(columnNames = {"name"})})
public class User implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull
	@Size (min=3, max=45)
	private String name;

	@OneToMany(mappedBy="owner")
	@JsonIgnore
	private Set<Creation> creationSet = new HashSet<>();

	@OneToMany(mappedBy = "shareHolder")
	@JsonIgnore
	private Set<Share> shareSet = new HashSet<>();


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull
	public String getName() {
		return name;
	}

	public void setName(@NotNull String name) {
		this.name = name;
	}

	public Set<Creation> getCreationSet() {
		return creationSet;
	}

	public void setCreationSet(Set<Creation> creations) {
		this.creationSet = creations;
	}

	public Set<Share> getShareSet() {
		return shareSet;
	}

	public void setShareSet(Set<Share> shareSet) {
		this.shareSet = shareSet;
	}

	public String toString() {
		return "User{ id = " + id + ", name = " + name + "}";
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof User && Objects.equals(id, ((User) obj).id);
	}

	public enum Type{
		Artist, Publisher;
	}

}
