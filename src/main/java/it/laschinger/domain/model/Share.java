package it.laschinger.domain.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name="share", uniqueConstraints={@UniqueConstraint(columnNames = {"share_holder_id", "creation_id"})})
public class Share implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull
	@ManyToOne
	private User shareHolder;

	@NotNull
	@ManyToOne
	@JsonBackReference
	private Creation creation;

	@NotNull
	private BigDecimal quotient;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getShareHolder() {
		return shareHolder;
	}

	public void setShareHolder(@NotNull User shareHolder) {
		this.shareHolder = shareHolder;
	}

	public Creation getCreation() {
		return creation;
	}

	public void setCreation(@NotNull Creation creation) {
		this.creation = creation;
	}

	public BigDecimal getQuotient() {
		return quotient;
	}

	public void setQuotient(@NotNull BigDecimal quotient) {
		this.quotient = quotient;
	}

	public String toString() {
		return "Share{ id = " + id + ", shareHolder = " + shareHolder + ", creation=" + creation + "}";
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof Share && Objects.equals(id, ((Share) obj).id);
	}
}
