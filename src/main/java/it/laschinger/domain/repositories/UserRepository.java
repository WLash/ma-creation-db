package it.laschinger.domain.repositories;

import it.laschinger.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface UserRepository extends JpaRepository<User, Long> {
	User findById(Long id);
}