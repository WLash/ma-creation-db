package it.laschinger.domain.repositories;

import it.laschinger.domain.model.Creation;
import it.laschinger.domain.model.Share;
import it.laschinger.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShareRepository extends JpaRepository<Share, Long> {
	Share findById(Long shareId);
}