package it.laschinger.domain.repositories;

import it.laschinger.domain.model.Creation;
import it.laschinger.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CreationRepository extends JpaRepository<Creation, Long> {
	Creation findById(Long id);
}