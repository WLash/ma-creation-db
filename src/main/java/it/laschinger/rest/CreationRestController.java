package it.laschinger.rest;

import it.laschinger.domain.model.Creation;
import it.laschinger.domain.model.Greeting;
import it.laschinger.domain.model.Share;
import it.laschinger.domain.model.User;
import it.laschinger.domain.repositories.CreationRepository;
import it.laschinger.domain.repositories.ShareRepository;
import it.laschinger.domain.repositories.UserRepository;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicLong;

@RestController
class CreationRestController {

	private static Log logger = LogFactory.getLog(CreationRestController.class);


	private final UserRepository userRepository;
	private final CreationRepository creationRepository;
	private final ShareRepository shareRepository;

	@Autowired
	public CreationRestController(UserRepository userRepository,
								  CreationRepository creationRepository,
								  ShareRepository shareRepository) {
		this.userRepository = userRepository;
		this.creationRepository = creationRepository;
		this.shareRepository = shareRepository;
	}

	@RequestMapping("/users")
	public Collection<User> getUsers() {
		logger.debug("users: ");
		return userRepository.findAll();
	}

	@RequestMapping("/user/{userId}")
	public User getUser(@PathVariable Long userId){
		logger.debug("user: " + userId);
		return userRepository.findById(userId);
	}

	@RequestMapping("/creation/{creationId}")
	public Creation getCreation(@PathVariable Long creationId){
		logger.debug("creation: " + creationId);
		return creationRepository.findById(creationId);
	}

	@RequestMapping("/share/{shareId}")
	public Share getShare(@PathVariable Long shareId){
		logger.debug("share: " + shareId);
		return shareRepository.findById(shareId);
	}

	@RequestMapping(value = "/user/add", method = RequestMethod.POST)
	public User addUser(@RequestBody User user){
		final User savedUser = userRepository.save(user);
		return savedUser;
	}

	@RequestMapping(value ="/creation/add", method = RequestMethod.POST)
	public Creation addCreation(@RequestBody Creation creation){
		return creationRepository.save(creation);
	}

	@RequestMapping(value="/share/add", method = RequestMethod.POST)
	public Share addShare(@RequestBody Share share){
		return shareRepository.save(share);
	}

	@ExceptionHandler(value = { Exception.class })
	public void unknownException(Exception ex, WebRequest req) {
		ex.printStackTrace();
	}
}