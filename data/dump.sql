-- MySQL dump 10.16  Distrib 10.3.8-MariaDB, for osx10.13 (x86_64)
--
-- Host: 127.0.0.1    Database: creation_db
-- ------------------------------------------------------
-- Server version	10.3.8-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `creation`
--

DROP TABLE IF EXISTS `creation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `owner_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKwfkxyljx1i512grjj9vg5asc` (`title`,`owner_id`),
  KEY `FKo0jtcerm8a93k4g9op8yetdmt` (`owner_id`),
  CONSTRAINT `FKo0jtcerm8a93k4g9op8yetdmt` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creation`
--

LOCK TABLES `creation` WRITE;
/*!40000 ALTER TABLE `creation` DISABLE KEYS */;
INSERT INTO `creation` VALUES (4,'Alles wegen Dir',2);
INSERT INTO `creation` VALUES (3,'Am Ende',2);
INSERT INTO `creation` VALUES (11,'DNA.',6);
INSERT INTO `creation` VALUES (9,'Don\'t Look Back In Anger',5);
INSERT INTO `creation` VALUES (7,'Hurt Less',4);
INSERT INTO `creation` VALUES (1,'Let It Be',1);
INSERT INTO `creation` VALUES (10,'Little By Little',5);
INSERT INTO `creation` VALUES (12,'LOVE.',6);
INSERT INTO `creation` VALUES (5,'Mr. Brightside',3);
INSERT INTO `creation` VALUES (6,'Run for Cover',3);
INSERT INTO `creation` VALUES (2,'Songs für Liam',2);
INSERT INTO `creation` VALUES (8,'Sour Breath',4);
/*!40000 ALTER TABLE `creation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `share`
--

DROP TABLE IF EXISTS `share`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `share` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `creation_id` bigint(20) DEFAULT NULL,
  `share_holder_id` bigint(20) DEFAULT NULL,
  `quotient` decimal(5,2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKm5voik094mdoudigmhp2t8m6l` (`share_holder_id`,`creation_id`),
  KEY `FKk1kbgfybwfvq0var9ght1qogb` (`creation_id`),
  CONSTRAINT `FKk1kbgfybwfvq0var9ght1qogb` FOREIGN KEY (`creation_id`) REFERENCES `creation` (`id`),
  CONSTRAINT `FKmyv1rvyb6qb6dhhmw01f913qh` FOREIGN KEY (`share_holder_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `share`
--

LOCK TABLES `share` WRITE;
/*!40000 ALTER TABLE `share` DISABLE KEYS */;
INSERT INTO `share` VALUES (1,1,8,100.00);
INSERT INTO `share` VALUES (2,2,11,4.50);
INSERT INTO `share` VALUES (3,2,12,3.25);
INSERT INTO `share` VALUES (4,2,13,3.25);
INSERT INTO `share` VALUES (5,2,14,45.00);
INSERT INTO `share` VALUES (6,2,15,44.00);
INSERT INTO `share` VALUES (7,3,11,4.00);
INSERT INTO `share` VALUES (8,3,12,3.00);
INSERT INTO `share` VALUES (9,3,13,3.00);
INSERT INTO `share` VALUES (10,3,14,45.00);
INSERT INTO `share` VALUES (11,3,15,45.00);
INSERT INTO `share` VALUES (12,4,11,3.00);
INSERT INTO `share` VALUES (13,4,12,4.00);
INSERT INTO `share` VALUES (14,4,13,3.00);
INSERT INTO `share` VALUES (15,4,14,45.00);
INSERT INTO `share` VALUES (16,4,15,45.00);
INSERT INTO `share` VALUES (17,5,16,4.00);
INSERT INTO `share` VALUES (18,5,17,2.50);
INSERT INTO `share` VALUES (19,5,18,2.25);
INSERT INTO `share` VALUES (20,5,19,91.25);
INSERT INTO `share` VALUES (21,6,16,3.75);
INSERT INTO `share` VALUES (22,6,17,2.75);
INSERT INTO `share` VALUES (23,6,18,2.25);
INSERT INTO `share` VALUES (24,6,19,91.25);
INSERT INTO `share` VALUES (25,7,4,41.00);
INSERT INTO `share` VALUES (26,7,20,59.00);
INSERT INTO `share` VALUES (27,8,4,41.25);
INSERT INTO `share` VALUES (28,8,20,58.75);
INSERT INTO `share` VALUES (29,9,5,10.00);
INSERT INTO `share` VALUES (30,9,21,11.00);
INSERT INTO `share` VALUES (31,9,22,79.00);
INSERT INTO `share` VALUES (32,10,5,10.50);
INSERT INTO `share` VALUES (33,10,21,11.50);
INSERT INTO `share` VALUES (34,10,22,78.00);
INSERT INTO `share` VALUES (35,11,6,13.00);
INSERT INTO `share` VALUES (36,11,23,87.00);
INSERT INTO `share` VALUES (37,12,6,19.00);
INSERT INTO `share` VALUES (38,12,23,81.00);
/*!40000 ALTER TABLE `share` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKgj2fy3dcix7ph7k8684gka40c` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (8,'Apple Corps Ltd.');
INSERT INTO `user` VALUES (15,'BMG Rights Management GmbH');
INSERT INTO `user` VALUES (16,'Brandon Flowers');
INSERT INTO `user` VALUES (17,'Dave Brent Kuening');
INSERT INTO `user` VALUES (11,'Felix Brummer');
INSERT INTO `user` VALUES (4,'Julien Baker');
INSERT INTO `user` VALUES (12,'Karl Schumann');
INSERT INTO `user` VALUES (6,'Kendrick Lamar');
INSERT INTO `user` VALUES (20,'Kobalt Music Publishing Ltd.');
INSERT INTO `user` VALUES (2,'Kraftklub');
INSERT INTO `user` VALUES (14,'Kraftklub Musikverlag GbR');
INSERT INTO `user` VALUES (18,'Mark August Stoermer');
INSERT INTO `user` VALUES (21,'Noel Gallagher');
INSERT INTO `user` VALUES (5,'Oasis');
INSERT INTO `user` VALUES (13,'Philipp Hoppen');
INSERT INTO `user` VALUES (22,'Sony/ATV Music Publishing Ltd.');
INSERT INTO `user` VALUES (1,'The Beatles');
INSERT INTO `user` VALUES (3,'The Killers');
INSERT INTO `user` VALUES (19,'Universal Music Publishing Ltd.');
INSERT INTO `user` VALUES (23,'Warner/Chapell Music');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-30 21:56:20
